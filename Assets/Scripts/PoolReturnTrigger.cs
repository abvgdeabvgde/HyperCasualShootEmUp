using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class PoolReturnTrigger : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.TryGetComponent<PoolableMono>(out var poolable))
        {
            poolable.Return();
        }
        else
        {
            Destroy(collision.gameObject);
        }
    }
}
