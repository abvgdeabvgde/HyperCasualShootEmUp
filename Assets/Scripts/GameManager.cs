using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private ScoreManager scoreManager;
    [SerializeField] private EnemySpawner enemySpawner;
    [SerializeField] private Player player;

    private void Awake()
    {
        enemySpawner.EnemyDied += OnEnemyDeath;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RespawnPlayer();
        }
    }

    private void RespawnPlayer()
    {
        if (!player.gameObject.activeInHierarchy)
        {
            player.gameObject.SetActive(true);
            scoreManager.Score = 0;
            enemySpawner.RemoveAll();
        }
    }

    private void OnEnemyDeath()
    {
        scoreManager.Score++;
    }
}
