﻿using UnityEngine;

public class ScoreManager : Singleton<ScoreManager>
{
    [SerializeField] private TMPro.TextMeshProUGUI scoreText;
    private int score;

    public int Score
    {
        get => score;
        set
        {
            score = value;
            scoreText.text = "Score\n" + value.ToString();
        }
    }
}
