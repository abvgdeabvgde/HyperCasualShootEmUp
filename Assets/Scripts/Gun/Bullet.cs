﻿using UnityEngine;

public class Bullet : PoolableMono
{
    public int damage;
    public float speed;

    private void Update()
    {
        transform.Translate(0, speed * Time.deltaTime, 0);
    }

    public override void OnReset()
    {
        speed = 0;
        transform.position = Vector3.zero;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent<IDamageable>(out var damageable))
        {
            damageable.Damage(damage);
            Return();
        }
    }

    private void OnBecameInvisible() => Return();

    public override void Return() => Pool<Bullet>.Instance.Return(this);
}