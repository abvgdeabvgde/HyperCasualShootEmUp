﻿using UnityEngine;

public class BulletManager : Pool<Bullet>
{
    public int damage = 1;
    public float speed = 20f;

    public Bullet Spawn(Vector3 position)
    {
        var bullet = Get();
        bullet.gameObject.SetActive(true);
        bullet.speed = speed;
        bullet.damage = damage;
        bullet.transform.position = position;
        return bullet;
    }
}