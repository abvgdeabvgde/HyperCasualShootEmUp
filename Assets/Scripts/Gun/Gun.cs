﻿using UnityEngine;

public class Gun : MonoBehaviour, ISpawner<Bullet>
{
    [SerializeField] BulletManager bulletManager;
    [SerializeField] private float cooldown = 0.4f;

    private float timer;
    private bool CanShoot => timer < 0;

    private void Update()
    {
        timer -= Time.deltaTime;
    }

    private Bullet Shoot()
    {
        timer = cooldown;
        return bulletManager.Spawn(transform.position);
    }

    public bool TrySpawn(out Bullet obj)
    {
        obj = CanShoot ? Shoot() : null;
        return obj != null;
    }
}