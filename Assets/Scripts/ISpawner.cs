﻿public interface ISpawner<T>
{
    bool TrySpawn(out T obj);
}
