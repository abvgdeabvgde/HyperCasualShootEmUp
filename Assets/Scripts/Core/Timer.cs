using System;

public struct Timer
{
    private event Action Completed;
    private event Action<float> OnTick;

    public float Duration { get; set; }
    public float Time { get; private set; }
    public float Progress { get => Time / Duration; }
    public bool Repeat { get; set; }
    public bool IsRunning { get; set; }

    public Timer(float duration, Action Completed, bool repeat = true, bool startRunning = true, Action<float> OnTick = null)
    {
        Duration = duration;
        Time = 0;
        this.Completed = Completed;
        this.OnTick = OnTick;
        this.Repeat = repeat;
        this.IsRunning = startRunning;
    }

    public void Update(float dt)
    {
        if (!IsRunning)
        {
            return;
        }

        Time =
            Time + dt > Duration ? Duration :
            Time + dt < 0 ? 0 :
            Time + dt;

        if (Time >= Duration)
        {
            Complete();
        }
        else
        {
            OnTick?.Invoke(Progress);
        }
    }

    public void Pause()
    {
        IsRunning = false;
    }

    public void Stop()
    {
        IsRunning = false;
        Time = 0f;
    }

    public void Resume()
    {
        IsRunning = true;
    }

    public void Restart()
    {
        IsRunning = true;
        Time = 0;
    }

    public void Rewind(float t)
    {
        Time += t;
    }

    public void Complete()
    {
        Completed?.Invoke();

        if (Repeat)
        {
            Time -= Duration;
        }
    }
}
