public class OnScreenInput : Singleton<OnScreenInput>
{
    public float Horizontal { get; protected set; }

    public float Vertical { get; protected set; }

    public void Left(bool down) => Horizontal = down ? -1 : 0;

    public void Right(bool down) => Horizontal = down ? 1 : 0;

    public void Jump(bool down) => Vertical = down ? 1 : 0;
}