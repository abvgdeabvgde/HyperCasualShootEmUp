﻿using System;
using System.Collections;
using System.Text;
using UnityEngine;

public static class Helper
{
    public static Vector3 PointOnCircle(Vector3 center, float radius, float radians)
    {
        return new Vector3(
             center.x + (radius * Mathf.Cos(radians)),
             center.y + (radius * Mathf.Sin(radians)),
             center.z);
    }

    #region Layers
    /// <summary>
    /// Compares LayerMask.value and GameObject.layer.
    /// </summary>
    /// <returns>Returns true if layers are equal.</returns>
    public static bool EqualsLayer(this LayerMask layerMask, int gameObjectLayer)
    {
        return (layerMask.value & 1 << gameObjectLayer) == 1 << gameObjectLayer;
    }

    /// <summary>
    /// Converts Unity LayerMask to integer. Doesn't work if LayerMask has multiple layers selected.
    /// </summary>
    /// <returns>Returns integer similar to GameObject.layer.</returns>
    public static int ToIntLayer(this LayerMask layerMask)
    {
        int layerNumber = 0;
        int layer = layerMask.value;
        while (layer > 0)
        {
            layer >>= 1;
            layerNumber++;
        }

        return layerNumber - 1;
    }

    /// <summary>
    /// Excludes layers from LayerMask
    /// </summary>
    /// <returns>Returns resulting LayerMask without specified layers.</returns>
    public static LayerMask Remove(this LayerMask layerMask, params LayerMask[] layers)
    {
        int exclusionMask = 0;
        for (int i = 0; i < layers.Length; i++)
        {
            exclusionMask |= layers[i];
        }

        return layerMask & (~exclusionMask);
    }

    // Debug.Log("Original mask: " + ((LayerMask)layerMask).ToFormattedString());
    // Debug.Log("Exclusion mask: " + ((LayerMask)~exclusionMask).ToFormattedString());
    // Debug.Log("Result mask: " + ((LayerMask)(layerMask & (~exclusionMask))).ToFormattedString());
    #endregion

    /// <summary>
    /// Instantiates prefabs on grid with specified bounds.
    /// </summary>
    /// <param name="count">Total amount of elements.</param>
    /// <param name="objectsPerRow">Maximum amount of elements per row.</param>
    /// <param name="bounds">Bounding box to fit all elements into.</param>
    /// <param name="scale">How much of space elements takes up. The bigger number means closer distance between elements.</param>
    /// <param name="parent">Holder object for all created prefab instances.</param>
    /// <param name="callback">Called when GameObjects are being instantiated.</param>
    public static void InstantiateOnGrid(GameObject prefab, int count, int objectsPerRow, Vector2 bounds, float scale, Transform parent, Action<GameObject> callback)
    {
        objectsPerRow = objectsPerRow < count ? objectsPerRow : count;

        int remainder = count % objectsPerRow;
        int widthCount = objectsPerRow;
        int heightCount = (int)Mathf.Ceil((float)count / objectsPerRow);

        float maxWidth = bounds.x / widthCount;
        float maxHeight = bounds.y / heightCount;
        float preferedSize = Mathf.Min(maxWidth, maxHeight);

        for (int y = 0; y < heightCount; y++)
        {
            int objectCount = y == heightCount - 1 & remainder > 0 ? remainder : widthCount;

            for (int x = 0; x < objectCount; x++)
            {
                float remainderOffset = (objectsPerRow - objectCount) / 2f;
                float xOffset = (widthCount / 2f) - 0.5f;
                float yOffset = (heightCount / 2f) - 0.5f;
                Vector3 point = parent.TransformPoint(new Vector3(x + remainderOffset - xOffset, y - yOffset) * preferedSize);
                GameObject go = GameObject.Instantiate(prefab, point, Quaternion.identity, parent);
                go.transform.localScale = preferedSize * scale * Vector3.one;
                callback?.Invoke(go);
            }
        }
    }

    #region Coroutines
    /// <summary>
    /// Linearly interpolates between values using <i>float</i> t in a coroutine.
    /// </summary>
    /// <param name="time">Starting time.</param>
    public static IEnumerator LerpCoroutine(float duration, Action<float> action, float time = 0f, bool unscaledTime = false)
    {
        do
        {
            time = Mathf.Clamp(time + (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime), 0f, duration);
            action?.Invoke(time / duration);
            yield return null;
        } while (time < duration);
    }

    public static IEnumerator DelayedActionCoroutine(float duration, Action action, bool unscaledTime = false)
    {
        if (unscaledTime)
            yield return new WaitForSecondsRealtime(duration);
        else
        {
            yield return new WaitForSeconds(duration);
        }

        action?.Invoke();
    }
    #endregion

    public static void Add(this LineRenderer lineRenderer, Vector3 position)
    {
        lineRenderer.positionCount++;
        lineRenderer.SetPosition(lineRenderer.positionCount - 1, position);
    }

    /// <summary>
    /// Does action with each LineRenderer position.
    /// </summary>
    public static void ForEach(this LineRenderer lineRenderer, Action<Vector3> action)
    {
        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            action?.Invoke(lineRenderer.GetPosition(i));
        }
    }

    public static Vector3 Clamp(this ref Vector3 value, Vector3 min, Vector3 max)
    {
        value.x = Mathf.Clamp(value.x, min.x, max.x);
        value.y = Mathf.Clamp(value.y, min.y, max.y);
        value.z = Mathf.Clamp(value.z, min.z, max.z);
        return value;
    }

    public static Vector3 GetPosition(Vector3 origin, Vector3 direction, float distance)
    {
        return origin + (direction * distance);
    }

    /// <summary>
    /// Test before usage. May needs to be fixed to <i>direction * Vector3.one</i>.
    /// </summary>
    public static Vector3 GetPosition(Vector3 origin, Quaternion direction, float distance)
    {
        return GetPosition(origin, direction * Vector3.forward, distance);
    }

    public static Quaternion Lerp(this Quaternion q, Quaternion quaternion, float t)
    {
        return Quaternion.Lerp(q, quaternion, t);
    }

    /// <summary>
    /// Formats LayerMask to colored string.
    /// </summary>
    /// <returns>Returns string suitable for Debug.Log().</returns>
    public static string ToFormattedString(this LayerMask mask)
    {
        var layers = new StringBuilder();
        for (int i = 0; i < 32; i++)
        {
            layers.AppendFormat("<color={0}>{1}</color>|", (mask.value >> i & 1) == 1 ? "green" : "red", LayerMask.LayerToName(i));
            //// layers += "<color=" + ((mask.value >> i & 1) == 1 ? "green" : "red") + ">" + LayerMask.LayerToName(i) + "</color>|";
        }

        return layers.ToString();
    }
}
