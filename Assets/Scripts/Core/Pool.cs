﻿using System.Collections.Generic;
using UnityEngine;

public abstract class Pool<T> : Singleton<Pool<T>> where T : PoolableMono
{
    public T prefab;
    private Queue<T> queue = new Queue<T>();

    [SerializeField] [Min(0)] private int prewarmCount;
    [SerializeField] private bool debugLog = true;

    private Transform parent;
    public override void Awake()
    {
        base.Awake();
        parent = new GameObject($"Pool - {name}").transform;
        Add(prewarmCount);
    }

    public void Add(int count = 1)
    {
        for (int i = 0; i < count; i++)
        {
            if (debugLog)
            {
                Debug.Log($"{name}: Add");
            }

            var obj = Instantiate(prefab, parent);
            obj.gameObject.SetActive(false);
            queue.Enqueue(obj);
        }
    }

    public T Get()
    {
        if (debugLog)
        {
            Debug.Log($"{name}: Get");
        }

        if (queue.Count < 1)
        {
            Add();
        }

        return queue.Dequeue();
    }

    public void Return(T obj)
    {
        obj.OnReset();
        obj.gameObject.SetActive(false);

        queue.Enqueue(obj);
    }
}

public abstract class PoolableMono : MonoBehaviour
{
    public abstract void OnReset();

    public abstract void Return();
}
