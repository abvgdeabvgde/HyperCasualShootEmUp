using System;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Player : MonoBehaviour
{
    [SerializeField] private Gun gun;
    [SerializeField] private float minX;
    [SerializeField] private float maxX;

    private Camera cam;

    public event Action Died;

    private void Awake() => cam = Camera.main;

    private void Update()
    {
        Vector3 pos = transform.position;

        if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 1000))
        {
            pos.x = hit.point.x;
        }

        pos.x = Mathf.Clamp(pos.x, minX, maxX);
        transform.position = pos;

        gun.TrySpawn(out var bullet);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Enemy>(out var enemy))
        {
            Died?.Invoke();
            gameObject.SetActive(false);
        }
    }
}
