﻿using UnityEngine;

public class PlayerControlKeyboard : IControl
{
    public InputData GetInput()
    {
        var input = new InputData
        {
            direction = Input.GetAxis("Horizontal"),
            shoot = true
        };

        return input;
    }
}
