﻿using UnityEngine;

public class PlayerControlTouch : IControl
{
    private Camera camera;
    private Transform transform;

    public PlayerControlTouch(Camera camera, Transform transform)
    {
        this.camera = camera;
        this.transform = transform;
    }

    public InputData GetInput()
    {
        float direction = 0;

        if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 1000))
        {
            direction = transform.position.x < hit.point.x ? 1 : -1;
        }

        var input = new InputData
        {
            direction = direction,
            shoot = true
        };

        return input;
    }
}