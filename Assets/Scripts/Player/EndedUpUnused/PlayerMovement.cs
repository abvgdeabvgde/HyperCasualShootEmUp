﻿using UnityEngine;

public class PlayerMovement : IMovement
{
    private Transform transform;
    private float speed;
    private float minX;
    private float maxX;

    public PlayerMovement(Transform transform, float speed, float minX, float maxX)
    {
        this.transform = transform;
        this.speed = speed;
        this.minX = minX;
        this.maxX = maxX;
    }

    public void Move(InputData input, float deltaTime)
    {
        Vector3 pos = transform.position;
        pos.x += input.direction * speed * deltaTime;
        pos.x = Mathf.Clamp(pos.x, minX, maxX);
        transform.position = pos;
    }
}