﻿public interface IMovement
{
    void Move(InputData input, float deltaTime);
}