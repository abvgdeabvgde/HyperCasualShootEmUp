﻿public interface IControl
{
    InputData GetInput();
}