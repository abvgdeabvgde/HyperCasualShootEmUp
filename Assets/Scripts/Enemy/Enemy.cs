﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Enemy : PoolableMono, IDamageable
{
    [HideInInspector] public Rigidbody2D rigidbody;
    [SerializeField] private TMPro.TextMeshPro[] text;

    public HpSystem hpSystem;

    public event Action Died;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        hpSystem = new();
        hpSystem.Changed += UpdateText;
        hpSystem.Died += Died;
        hpSystem.Died += Return;
        // TODO: disgusting
        hpSystem.Died += () => ScoreManager.Instance.Score++;
    }

    private void UpdateText(int value)
    {
        for (int i = 0; i < text.Length; i++)
        {
            text[i].text = value.ToString();
        }
    }

    public override void OnReset()
    {
        rigidbody.velocity = Vector2.zero;
        rigidbody.angularVelocity = 0f;
    }

    public void Damage(int value) => hpSystem.Health -= value;

    public override void Return() => Pool<Enemy>.Instance.Return(this);
}
