﻿public abstract class EnemyFactory<T> : Pool<T>, ISpawner<T> where T : Enemy
{
    public abstract bool TrySpawn(out T obj);
}