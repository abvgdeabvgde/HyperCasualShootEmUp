﻿using UnityEngine;

public class SimpleEnemyFactory : EnemyFactory<Enemy>
{
    private const int Hp = 4;
    public override bool TrySpawn(out Enemy enemy)
    {
        float xForce = Random.Range(2f, 3f);
        xForce *= Random.value > 0.5 ? 1 : -1;

        Vector2 force = new(xForce, 0f);

        enemy = Get();
        enemy.gameObject.SetActive(true);
        enemy.rigidbody.AddForce(force, ForceMode2D.Impulse);
        enemy.hpSystem.Health = Hp;

        return enemy;
    }
}