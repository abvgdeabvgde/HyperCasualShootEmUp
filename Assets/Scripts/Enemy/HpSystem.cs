﻿using System;

public class HpSystem
{
    private int health;

    public event Action<int> Changed;
    public event Action Died;

    public int Health
    {
        get => health;
        set
        {
            health = value;

            Changed?.Invoke(health);
            if (Health <= 0)
            {
                Died?.Invoke();
            }
        }
    }
}