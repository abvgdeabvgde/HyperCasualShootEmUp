using System;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    private PatternData[] patterns = new PatternData[] {
        new()
        {
            enemyTypeIndex = 0,
            delay = 2f
        },
        new()
        {
            enemyTypeIndex = 0,
            delay = 2f
        },
        new()
        {
            enemyTypeIndex = 0,
            delay = 4f
        },
        new()
        {
            enemyTypeIndex = 1,
            delay = 2f
        }
    };

    [SerializeField] private EnemyFactory<Enemy>[] enemyFactories;
    private List<Enemy> enemies = new();

    private float timer;
    private int currentPatternIndex;

    public Vector3 RandomSpawnPosition => transform.position + (UnityEngine.Random.value > 0.5f ? Vector3.left : Vector3.right) * 2f;

    private int CurrentPatternIndex
    {
        get => currentPatternIndex;
        set => currentPatternIndex = value % patterns.Length;
    }

    private PatternData CurrentPattern => patterns[CurrentPatternIndex];

    private int CurrentEnemyFactoryIndex => Mathf.Clamp(CurrentPattern.enemyTypeIndex, 0, enemyFactories.Length - 1);

    private ISpawner<Enemy> CurrentEnemyFactory => enemyFactories[CurrentEnemyFactoryIndex];

    public event Action EnemyDied;

    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            timer += CurrentPattern.delay;
            CurrentPatternIndex++;

            if (CurrentEnemyFactory.TrySpawn(out var enemy))
            {
                enemies.Add(enemy);
                enemy.transform.position = RandomSpawnPosition;
                // TODO: doesnt work
                enemy.Died += () => Debug.Log("enemy.Died event"); //EnemyDied;
            }
        }
    }

    public void RemoveAll()
    {
        foreach (Enemy enemy in enemies)
        {
            enemy.Return();
        }

        enemies.Clear();
    }

    private struct PatternData
    {
        public int enemyTypeIndex;
        public float delay;
    }
}
